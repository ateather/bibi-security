#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "logtool.h"

#define LEAVE 'L'
#define ARRIVE 'A'

#define GUEST 'G'
#define EMPLOYEE 'E'

#define GALLERYID -1
#define NUM_MAX 0x3FFFFFFF

int invalid(){
  printf("invalid\n");
  if (exit_on_error) exit(255);
  return 0;
}

// return 0 if no error
int secure_parse_uint(char* str, unsigned int* num){
  *num = (unsigned int) atoi(str);
  char str2[20];
  sprintf(str2, "%u", *num);
  return strcmp(str, str2);
}

// return 0 if no error
int secure_parse_int(char* str, int* num){
  *num = atoi(str);
  char str2[20];
  sprintf(str2, "%d", *num);
  return strcmp(str, str2);
}

int isalnumstr(char* str){
  while(*str != '\0')
    if (!isalnum(*(str++)))
      return 0;
  return 1;
}

int isalphastr(char* str){
  while(*str != '\0')
    if(!isalpha(*(str++)))
      return 0;
  return 1;
}

int is_valid_filepath(char* str){
  char* filename = str;
  while(*str != '\0')
    if(*(str++) == '/')
      if(*(filename = str) == '\0')
        return 0;
  while(*filename != '\0')
    if (!isalnum(*filename) && *filename != '.' && *filename != '_')
      return 0;
    else
      filename++;
  return 1;
}

Person new_person(char type, char* name){
  Person p;
  p.type = type;
  // TODO: handle the case when name length too long
  if(!isalphastr(name)) invalid();
  strncpy(p.name, name, NAME_MAX_LENGTH);
  p.name[NAME_MAX_LENGTH - 1] = '\0';
  return p;
}
