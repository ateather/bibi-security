#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../build/logtool.h"

int person_cmp(Person p1, Person p2);
int record_cmp(Record r1, Record r2);
int entry_cmp(Entry e1, Entry e2);
int table_cmp(HashTable t1, HashTable t2);

int main(int argc, char** argv){
  char key[32] = "123456";
  Person jihan, anslem, jojo, roro, toto, coco;
  jihan.type = 'E';
  strcpy(jihan.name, "Jihan");
  anslem.type = 'E';
  strcpy(anslem.name, "Anslem");
  jojo.type = 'G';
  strcpy(jojo.name, "Jojo");
  roro.type = 'G';
  strcpy(roro.name, "Roro");
  toto.type = 'G';
  strcpy(toto.name, "Toto");
  coco.type = 'G';
  strcpy(coco.name, "Coco");

//   Record:
//   unsigned int time;
//   Person person;
//   char action;
//   int room;
  Record records[20] = {
    {1, jihan, 'A', -1},
    {2, jihan, 'A', 2},
    {3, jojo, 'A', -1},
    {4, roro, 'A', -1},
    {5, jihan, 'L', 2},
    {6, jojo, 'A', 4},
    {7, roro, 'A', 4},
    {8, anslem, 'A', -1},
    {9, roro, 'L', 4},
    {10, jihan, 'L', -1},
    {11, anslem, 'A', 2},
    {12, roro, 'A', 3},
    {13, jojo, 'L', 4},
    {14, jojo, 'A', 6},
    {15, anslem, 'L', 2},
    {16, roro, 'L', 3},
    {17, roro, 'L', -1},
    {18, jojo, 'L', 6},
    {19, jojo, 'L', -1},
    {20, anslem, 'L', -1}
  };

  unsigned int record_count = 20;

  unsigned indices_jihan[] = {0, 1, 4, 9};
  unsigned indices_jojo[] = {2, 5, 12, 13, 17, 18};
  unsigned indices_roro[] = {3, 6, 8, 11, 15, 16};
  unsigned indices_anslem[] = {7, 10, 14, 19};

// Entry:
// Person person;
// unsigned int num_records;
// unsigned int* indices;
  Entry entries[4] = {
    {jihan, 4, indices_jihan},
    {jojo, 6, indices_jojo},
    {roro, 6, indices_roro},
    {anslem, 4, indices_anslem}
  };

// HashTable:
// unsigned int size;
// Entry* entries;
  HashTable tbl;
  tbl.size = 4;
  tbl.entries = entries;

  Log* log = new_log();
  log -> table = tbl;
  log -> record_count = record_count;
  log -> record_list = records;

// Test 1 ====================================================================
  printf("Test 1: Entry <-> Binary convertion\n");
  char* entry_bin = NULL;
  unsigned int entry_bin_len = entry2binary(entries[0], &entry_bin);
  printf("Converted Entry to binary, size: %d\n", entry_bin_len);
  Entry e;
  binary2entry(entry_bin, &e);
  free(entry_bin);
  /*
  printf("Type Actual: %c, Expected: %c\n", e.person.type, entries[0].person.type);
  printf("Name Actual: %s, Expected: %s\n", e.person.name, entries[0].person.name);
  printf("Count Actual: %d, Expected: %d\n", e.num_records, entries[0].num_records);
  for(int i = 0; i < entries[0].num_records; i++)
    printf("indices[%d] Actual: %d, Expected: %d\n", i, e.indices[i], entries[0].indices[i]);
  */

  if(entry_cmp(e, entries[0]))
    printf("Test 1 failed!\n");
  else
    printf("Test 1 passed!\n");

// Test 2 ====================================================================
  printf("Test 2: HashTable <-> Binary convertion\n");
  char* table_bin = NULL;
  unsigned int table_bin_len = table2binary(tbl, &table_bin);
  printf("Converted HashTable to binary, size: %d\n", table_bin_len);
  HashTable t;
  binary2table(table_bin, &t);
  free(table_bin);

  if(table_cmp(t, tbl))
    printf("Test 2 failed!\n");
  else
    printf("Test 2 passed!\n");

// Test 3 ====================================================================
  printf("Test 3: Logfile Read & Write\n");
  Log* l3 = NULL;
  char fname3[20] = "f_test3.log";
  int test3result = 1;

  unsigned int fsize = write_log(fname3, key, log);
  printf("wrote log to file, size = %d\n", fsize);
  l3 = load_log(fname3, key);
  printf("log loaded from file, record count: %d\n", l3 -> record_count);

  if(table_cmp(tbl, l3 -> table)){
    printf("Test 3: mismatched hashtable\n");
    test3result = 0;
  }else{
    //printf("Test 3: hashtable matched\n");
  }
  if(l3 -> record_count != record_count){
    printf("Test 3: mismatched record count\n");
    printf("Expected: %d, Actual: %d\n", record_count, l3 -> record_count);
    test3result = 0;
  }else{
    //printf("Test 3: record count matched\n");
  }
  for(int i = 0; i < record_count; i++){
    if(record_cmp(l3 -> record_list[i], records[i])){
      printf("Test 3: mismatched record[%d]\n", i);
      test3result = 0;
    }else{
      //printf("Test 3: record[%d] matched\n", i);
    }
  }
  if(test3result)
    printf("Test 3 passed!\n");
  else
    printf("Test 3 failed!\n");

// Test 4 ====================================================================
  printf("Test 4: helper functions\n");
  int t4result = 1;
  Entry* e4 = get_entry_ptr(log, jihan);
  if(e4 != &(entries[0])){
    printf("Test 4: get_entry_ptr failed to return expected pointer\n");
    t4result = 0;
  }
  Record* r4 = get_last_record_of_person(log, jihan);
  if(r4 != &(records[9])){
    printf("Test 4: get_last_record_of_person failed to return expected pointer\n");
    t4result = 0;
  }
  Room* rooms = NULL;
  unsigned int room_count = get_all_room_status(log, &rooms);
  if(room_count != 0){
    printf("Test 4: room count not expected\n");
    t4result = 0;
  }
  free_rooms(rooms, room_count);

  if(t4result)
    printf("Test 4 passed!\n");
  else
    printf("Test 4 failed!\n");

// Test 5 ====================================================================
  printf("Test 5: Integration Test\n");
  int t5result = 1;
  Log* l5 = new_log();
/*
    {1, jihan, 'A', -1},
    {2, jihan, 'A', 2},
    {3, jojo, 'A', -1},
    {4, roro, 'A', -1},
    {5, jihan, 'L', 2},
    {6, jojo, 'A', 4},
    {7, roro, 'A', 4},
    {8, anslem, 'A', -1},
    {9, roro, 'L', 4},
    {10, jihan, 'L', -1},
*/
  for(int i = 0; i < 10; i++){
    append_record(l5, records[i]);
  }
  Entry* e5 = get_entry_ptr(l5, jihan);
  if(entry_cmp(*e5, entries[0])){
    printf("Test 5: get_entry_ptr for jihan failed\n");
    t5result = 0;
  }
  e5 = get_entry_ptr(l5, jojo);
  if(person_cmp(e5 -> person, jojo) && e5 -> indices[0] != 2 && e5 -> indices[1] != 5){
    printf("Test 5: get_entry_ptr for jojo failed\n");
    t5result = 0;
  }
  e5 = get_entry_ptr(l5, roro);
  if(person_cmp(e5 -> person, roro) && e5 -> indices[0] != 3 && e5 -> indices[1] != 6 && e5 -> indices[2] != 8){
    printf("Test 5: get_entry_ptr for roro failed\n");
    t5result = 0;
  }
  Record* r5 = get_last_record_of_person(l5, jihan);
  if(record_cmp(*r5, records[9])){
    printf("Test 5: get_last_record_of_person for jihan failed\n");
    t5result = 0;
  }
  r5 = get_last_record_of_person(l5, jojo);
  if(record_cmp(*r5, records[5])){
    printf("Test 5: get_last_record_of_person for jojo failed\n");
    t5result = 0;
  }
  r5 = get_last_record_of_person(l5, roro);
  if(record_cmp(*r5, records[8])){
    printf("Test 5: get_last_record_of_person for roro failed\n");
    t5result = 0;
  }
  r5 = get_last_record_of_person(l5, anslem);
  if(record_cmp(*r5, records[7])){
    printf("Test 5: get_last_record_of_person for anslem failed\n");
    t5result = 0;
  }
  Room* rm5 = NULL;
  unsigned int rc5 = get_all_room_status(l5, &rm5);
  if(rc5 != 2){
    printf("Test 5: get_all_room_status did not return expected room_count\n");
    printf("Expected: 2, Actual: %d\n", room_count);
  }
  printf("Expected:\n");
  printf("\tRoom -1: Roro, Anslem\n");
  printf("\tRoom 4: Jojo\n");
  printf("Actual:\n");
  for(int i = 0; i < rc5; i++){
    printf("\tRoom %d: ", rm5[i].room_id);
    for(int j = 0; j < rm5[i].num_person; j++)
      printf("%s, ", rm5[i].person_list[j].name);
    printf("\n");
  }
  free_rooms(rm5, rc5);
// File I/O ===========================
  char t5_fn[20] = "t5.log";
  write_log(t5_fn, key, l5);
  Log* l5_2 = load_log(t5_fn, key);
  if(table_cmp(l5 -> table, l5_2 -> table)){
    printf("Test 5: Table different in saved file\n");
    t5result = 0;
  }
  if(l5_2 -> record_count != l5 -> record_count){
    printf("Test 5: mismatched record count\n");
    printf("Expected: %d, Actual: %d\n", l5 -> record_count, l5_2 -> record_count);
    t5result = 0;
  }
  for(int i = 0; i < l5 -> record_count; i++){
    if(record_cmp(l5 -> record_list[i], l5_2 -> record_list[i])){
      printf("Test 3: mismatched record[%d]\n", i);
      t5result = 0;
    }
  }
  for(int i = 10; i < 20; i++){
    append_record(l5_2, records[i]);
  }
  e5 = get_entry_ptr(l5_2, jihan);
  if(entry_cmp(*e5, entries[0])){
    printf("Test 5.2: get_entry_ptr for jihan failed\n");
    t5result = 0;
  }
  e5 = get_entry_ptr(l5_2, jojo);
  if(entry_cmp(*e5, entries[1])){
    printf("Test 5.2: get_entry_ptr for jojo failed\n");
    t5result = 0;
  }
  e5 = get_entry_ptr(l5_2, roro);
  if(entry_cmp(*e5, entries[2])){
    printf("Test 5.2: get_entry_ptr for roro failed\n");
    t5result = 0;
  }
  e5 = get_entry_ptr(l5_2, anslem);
  if(entry_cmp(*e5, entries[3])){
    printf("Test 5.2: get_entry_ptr for anslem failed\n");
    t5result = 0;
  }
  r5 = get_last_record_of_person(l5_2, jihan);
  if(record_cmp(*r5, records[9])){
    printf("Test 5.2: get_last_record_of_person for jihan failed\n");
    t5result = 0;
  }
  r5 = get_last_record_of_person(l5_2, jojo);
  if(record_cmp(*r5, records[18])){
    printf("Test 5.2: get_last_record_of_person for jojo failed\n");
    t5result = 0;
  }
  r5 = get_last_record_of_person(l5_2, roro);
  if(record_cmp(*r5, records[16])){
    printf("Test 5.2: get_last_record_of_person for roro failed\n");
    t5result = 0;
  }
  r5 = get_last_record_of_person(l5_2, anslem);
  if(record_cmp(*r5, records[19])){
    printf("Test 5.2: get_last_record_of_person for anslem failed\n");
    t5result = 0;
  }
  if(get_all_room_status(l5_2, &rm5) != 0){
    printf("Test 5.2: get_all_room_status did not return correct room_count after append log loaded from file\n");
    t5result = 0;
  }
  if(t5result)
    printf("Test 5 passed!\n");
  else
    printf("Test 5 failed!\n");
  free_log(l5);
  free_log(l5_2);
  return 0;
}

int record_cmp(Record r1, Record r2){
  if(r1.time != r2.time) return -1;
  if(person_cmp(r1.person, r2.person)) return -1;
  if(r1.action != r2.action) return -1;
  return r1.room - r2.room;
}

int entry_cmp(Entry e1, Entry e2){
  if(person_cmp(e1.person, e2.person)) return -1;
  if(e1.num_records != e2.num_records) return e1.num_records - e2.num_records;
  for(int i = 0; i < e1.num_records; i++)
    if(e1.indices[i] != e2.indices[i])
      return -1;
  return 0;
}

int table_cmp(HashTable t1, HashTable t2){
  if(t1.size != t2.size) return t1.size - t2.size;
  for(int i = 0; i < t1.size; i++)
    if(entry_cmp(t1.entries[i], t2.entries[i]))
      return -1;
  return 0;
}
