#include <openssl/evp.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char key_wrong_msg[32]  = "integrity violation";
int exit_on_error = 1;

void integrity_violation(){
  printf("%s\n", key_wrong_msg);
  if (exit_on_error) exit(255);
}

int secureio_initialized = 0;

void secureio_init(){
  OpenSSL_add_all_algorithms();
  secureio_initialized = 1;
}

unsigned int hash(const char *digest_name, const char *input, const unsigned int input_len, char *output){
  unsigned int hash_len = 0;
  EVP_MD_CTX *ctx = EVP_MD_CTX_create();
  const EVP_MD *digest = EVP_get_digestbyname(digest_name);
  if(digest == NULL){
    printf("digest \"%s\" not found\n", digest_name);
    return -1;
  }
  EVP_DigestInit_ex(ctx, digest, NULL);
  EVP_DigestUpdate(ctx, input, input_len);
  EVP_DigestFinal_ex(ctx, (unsigned char*) output, &hash_len);
  EVP_MD_CTX_destroy(ctx);

  return hash_len;
}

unsigned int encrypt(const char *plaintext, const unsigned int plaintext_len, const char *key, const char *iv, char *ciphertext){

  EVP_CIPHER_CTX *ctx;
  int len;
  unsigned int ciphertext_len;

  ctx = EVP_CIPHER_CTX_new();
  EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, (unsigned char*) key, (unsigned char*) iv);
  EVP_EncryptUpdate(ctx, (unsigned char*) ciphertext, &len, (unsigned char*) plaintext, plaintext_len);
  ciphertext_len = len;
  EVP_EncryptFinal_ex(ctx, (unsigned char*) ciphertext + len, &len);
  ciphertext_len += len;
  EVP_CIPHER_CTX_free(ctx);
  ciphertext[ciphertext_len] = '\0';

  return ciphertext_len;
}

unsigned int decrypt(const char *ciphertext, const unsigned int ciphertext_len, const char *key, const char *iv, char *plaintext){

  EVP_CIPHER_CTX *ctx;
  int len;
  unsigned int plaintext_len;

  ctx = EVP_CIPHER_CTX_new();
  EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, (unsigned char*) key, (unsigned char*) iv);
  EVP_DecryptUpdate(ctx, (unsigned char*) plaintext, &len, (unsigned char*) ciphertext, ciphertext_len);
  plaintext_len = len;
  EVP_DecryptFinal_ex(ctx, (unsigned char*) plaintext + len, &len);
  plaintext_len += len;
  EVP_CIPHER_CTX_free(ctx);
  plaintext[plaintext_len] = '\0';

  return plaintext_len;
}

unsigned int read_file(const char* filename, const char* key, char** outtxt){
  if (!secureio_initialized) secureio_init();

  char key_hash[32];
  hash("sha256", key, strlen(key), key_hash);

  FILE* fp;
  char* fcontent;
  unsigned int fsize;
  char* plaintext;
  unsigned int plaintext_len;
  char hash_val[32];
  int hash_len;

  if((fp = fopen(filename, "r")) == NULL){
    return 0;
  }

  fseek(fp, 0, SEEK_END);
  fsize = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  fcontent = malloc(fsize + 1);
  plaintext = malloc(fsize + 1);
  fread(fcontent, fsize, 1, fp);
  fclose(fp);

  unsigned int *target_hash_len = (unsigned int*) fcontent;
  if((char*) target_hash_len >= fcontent + fsize) integrity_violation();
  char *target_hash = (char*) (target_hash_len + 1);
  if(target_hash >= fcontent + fsize) integrity_violation();
  unsigned int *cypher_size = (unsigned int*) (target_hash + *target_hash_len);
  if((char*) cypher_size >= fcontent + fsize) integrity_violation();
  char *cypher_txt = (char*) (cypher_size + 1);
  if(cypher_txt + *cypher_size - 1 >= fcontent + fsize) integrity_violation();

  plaintext_len = decrypt(cypher_txt, *cypher_size, key_hash, NULL, plaintext);
  hash_len = hash("sha256", plaintext, plaintext_len, hash_val);

  if(hash_len != *target_hash_len || memcmp(hash_val, target_hash, hash_len))
    integrity_violation();

  *outtxt = plaintext;

  free(fcontent);

  return plaintext_len;
}

unsigned int write_file(const char* filename, const char* key, const char* txt, const unsigned int txt_len){
  if (!secureio_initialized) secureio_init();

  char key_hash[32];
  hash("sha256", key, strlen(key), key_hash);

  char hash_val[32];
  char* ciphertext = malloc(txt_len + 32);
  unsigned int hash_len = hash("sha256", txt, txt_len, hash_val);
  unsigned int ciphertext_len = encrypt(txt, txt_len, key_hash, NULL, ciphertext);
  unsigned int file_size = 0;

  FILE* fp;
  if((fp = fopen(filename, "w")) == NULL){
    return 0;
  }
  fwrite(&hash_len, sizeof(hash_len), 1, fp);
  fwrite(hash_val, sizeof(char), hash_len, fp);
  fwrite(&ciphertext_len, sizeof(ciphertext_len), 1, fp);
  fwrite(ciphertext, sizeof(char), ciphertext_len, fp);
  fclose(fp);
  free(ciphertext);

  file_size += sizeof(hash_len);
  file_size += sizeof(char) * hash_len;
  file_size += sizeof(ciphertext_len);
  file_size += sizeof(char) * ciphertext_len;
  return file_size;
}
