#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wordexp.h>

#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include "common.h"

int ACCEPTING_B = 1;

int main(int argc, char *argv[]);

int isPersonInGallery(Log *log, Person person) {
  Record *Current = get_last_record_of_person(log, person);
  if (!Current)  return 0;
  return Current->action != LEAVE || Current->room != GALLERYID;
}

int isActionValid(Log *log, Record record) {
  Person person = record.person;
  char action = record.action;
  int room = record.room;
  unsigned int timestamp = record.time;
  unsigned int last_time = get_last_record_time(log);
  // printf("-Last Entry Time: %d\n", last_time);
  if (timestamp <= last_time) return 0;
  Record *last_record = get_last_record_of_person(log, person);
  if (!last_record) return (action == ARRIVE && room == GALLERYID);
  // printf("-Record found: %c in %d @ %d\n", last_record->action, last_record->room, last_record->time );
  if (action == ARRIVE) {
    if (isPersonInGallery(log, person)) {
      // printf("-Person is in gallery\n");
      return ((last_record->action == LEAVE && room != GALLERYID)
        || (last_record->action == ARRIVE && last_record->room == GALLERYID && room != GALLERYID));
    } else {
      // printf("-Person is not in gallery\n");
      return (room == GALLERYID);
    }
  } else {
    // LEAVE
    if (!isPersonInGallery(log, person)) return 0;
      // printf("-Person is in gallery\n");
    if (last_record->action == ARRIVE)
      return (last_record->room == room);
    else if (last_record->action == LEAVE)
      return (room == GALLERYID);
  }
  return 0;
}


int parse_cmdline(int argc, char *argv[], unsigned int *timestamp, char **token,
  Person *cur_person, char *action, int *roomid, char **logpath, char **batchpath) {

  int is_good = 0;
  //pick up the switches
  int i;
  for(i = 1; i < argc; i++){
    if(!strcmp(argv[i], "-B")){
      //batch file
      if (!ACCEPTING_B)
        return 0;
      if(++i >= argc) return 0;
      *batchpath = argv[i];
      is_good = 1;
    }else if(!strcmp(argv[i], "-T")){
      //timestamp
      // printf("wut: %s\n", optarg );
      if(++i >= argc) return 0;
      if(secure_parse_uint(argv[i], timestamp)) return 0;
    }else if(!strcmp(argv[i], "-K")){
      //secret token
      if(++i >= argc) return 0;
      *token = argv[i];
    }else if(!strcmp(argv[i], "-A")){
      //arrival
      if(*action == LEAVE) return 0;
      *action = ARRIVE;
    }else if(!strcmp(argv[i], "-L")){
      //departure
      if(*action == ARRIVE) return 0;
      *action = LEAVE;
    }else if(!strcmp(argv[i], "-E")){
      //employee name
      if(cur_person -> type == GUEST) return 0;
      if(++i >= argc) return 0;
      *cur_person = new_person(EMPLOYEE, argv[i]);
    }else if(!strcmp(argv[i], "-G")){
      //guest name
      if(cur_person -> type == EMPLOYEE) return 0;
      if(++i >= argc) return 0;
      *cur_person = new_person(GUEST, argv[i]);
    }else if(!strcmp(argv[i], "-R")){
      //room ID
      if(++i >= argc) return 0;
      if(secure_parse_int(argv[i], roomid)) return 0;
    }else{
      *logpath = argv[i];
      is_good = 1;
    }
  }
  return is_good;
}

int batch_mode(char *cur_line, char *argv0) {
  if (cur_line == NULL || strlen(cur_line) < 1) return 0;
  // Clean current line
  // printf("%s", cur_line);
  size_t cur_len = strlen(cur_line);
  if (cur_line[cur_len - 1] == '\n') cur_line[cur_len - 1] = '\0';

  // Get commands parameters
  //char *params[12];
  size_t param_count = 1;
  char *token;
  char **params = malloc(param_count * sizeof(char*)); // How can this be 1 and not seg fault?? <- why seg fault?? malloc works with any non-negative integer
  params[param_count - 1] = argv0;
  token = strtok(cur_line, " ");
  while(token != NULL) {
    //printf("%d:%s\n",tokens, token);
    params = realloc(params, ++param_count * sizeof(char*));
    params[param_count - 1] = token;
    token = strtok(NULL, " ");
  }
  //printf("%s %s\n", params[1], params[2]);

  // Send to main
  if(param_count > 1) main(param_count, params);

  // Clean memory
  free(params);

  return 0;
}

int main(int argc, char *argv[]) {

  unsigned int timestamp = 0;
  int roomid = GALLERYID;
  Person cur_person = {0};
  char action = '\0';
  char *logpath = NULL;
  char *token = NULL;
  char *batchpath = NULL;

  //printf("%d %s\n", argc, argv[2] );

  strncpy(key_wrong_msg, "invalid", 32);
  key_wrong_msg[31] = '\0';

  if(!parse_cmdline(argc, argv, &timestamp, &token, &cur_person, &action, &roomid, &logpath, &batchpath)) return invalid();
  if (batchpath && !(timestamp || token || cur_person.type > 0 || action || roomid >= 0 || logpath)) {
    ACCEPTING_B = 0;

    FILE * fp = NULL;
    char *cur_line = NULL;
    size_t len = 0;

    fp = fopen(batchpath, "r");
    if (fp == NULL) return invalid();
    exit_on_error = 0;

    while (getline(&cur_line, &len, fp) != -1){
      batch_mode(cur_line, argv[0]);
      //free(cur_line);
    }
    free(cur_line);
    return 0;
  }

  //printf("here\n");
  //printf("Valid Inputs: %d %s %d %s\n", result, token, timestamp, logpath);
  // Decide if parameters are legal -- cur_person and action not checked !!
  if (!(token && isalnumstr(token) && timestamp && timestamp <= NUM_MAX && roomid <= NUM_MAX && logpath && is_valid_filepath(logpath))) return invalid();

  Record NewRecord;
  NewRecord.time = timestamp;
  NewRecord.person = cur_person;
  NewRecord.action = action;
  NewRecord.room = roomid;
  // Get Current Log info
  Log *log = load_log(logpath, token);

  if (log == NULL) log = new_log();

  if (!isActionValid(log, NewRecord)) return invalid();

  // Update the record list
  append_record(log, NewRecord);

  // Log to file
  // printf("%s has %c the gallery at: %d\n", cur_person.name, action, timestamp);
  if(!write_log(logpath, token, log)) return invalid();
  free_log(log);
  return 0;
}
