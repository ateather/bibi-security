#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wordexp.h>
#include <time.h>
#include <ctype.h>
#include <openssl/evp.h> 
#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>

int parse_cmdline(int argc, char *argv[]) {

  int opt = -1, i;
  int is_good = -1;
  int timestamp_bool = 0, arrival = 0, departure = 0, room = -1, timestamp = -1, not_new = 0;
  unsigned int len, token_len, app_str_len = 1;
  unsigned char *token = NULL, *employee = NULL, *guest = NULL, *logpath, *append;
  unsigned char salt[32], hash[128], *app_str, room_str[256], timestamp_str[12], *log;
  EVP_MD_CTX *ctx; 
  FILE *fp;
  FILE *fp3;


  //pick up the switches
  while ((opt = getopt(argc, argv, "T:K:E:G:ALR:B:")) != -1) {
    switch(opt) {
      case 'B':
        //batch file
        printf("unimplemented");
        exit(0);
        break;

      case 'T':
        //timestamp
        app_str_len += strlen(optarg);
        timestamp = atoi(optarg);
        //timestamp_str = malloc(strlen(optarg) + 1);
        timestamp_bool = 1;
        break;

      case 'K':
        //secret token
        token = malloc(strlen(optarg) + 33);
        strcpy(token, optarg);
        token[strlen(optarg)] = '\0';
        break;

      case 'A':
        //arrival
        app_str_len+=7;
        arrival = 1;
        break;

      case 'L':
        //departure
        app_str_len += 7;
        departure = 1; 
        break;

      case 'E':
        //employee name
        if(strlen(optarg) > 500) {
          printf("invalid\n");
          exit(255);
        }
        app_str_len += strlen(optarg) + 1 + 8;
        employee = malloc(strlen(optarg) + 1);
        strcpy(employee, optarg);
        employee[strlen(optarg)] = '\0';
        break;

      case 'G':
        //guest 
        if(strlen(optarg) > 500) {
          printf("invalid");
          exit(255);
        }
        app_str_len += strlen(optarg) + 1 + 5;
        guest = malloc(strlen(optarg) + 1);
        strcpy(guest, optarg);
        guest[strlen(optarg)] = '\0';
        break;

      case 'R':
        //room ID
        app_str_len += strlen(optarg);
        room = atoi(optarg);
        break;

      default:
        //unknown option, leave
        printf("invalid");
        exit(255);
        break;
    }

  }
  if(!timestamp_bool || token == NULL || room > 1073741823 || room < -1 || ((timestamp_bool) && (timestamp > 1073741823 || timestamp < 1))) {
    printf("invalid");
    exit(255);
  }
  for(i = 0; i < strlen(token); i++) {
    if(!((token[i] >= 'a' && token[i] <= 'z') || (token[i] >= 'A' && token[i] <= 'Z') || isdigit(token[i]))) {
      printf("invalid");
      exit(255);
    }
  }
  
  //pick up the positional argument for log path
  if(optind < argc) {
    logpath = argv[optind];
    log = malloc(strlen(logpath) + 1);
  }
  if(room == -1) {
    app_str_len += 3;
  }
  if(access(logpath, F_OK) != -1) {
    //file already , then decrypt
    unsigned char get_hash[67], get_salt[34], hash_str[65] = "", curr[3] = "";

    //EVP_CIPHER_CTX *ctx;
    //ctx=EVP_CIPHER_CTX_new();
    //EVP_DecryptInit_ex(ctx,EVP_aes_128_cbc(),NULL,keyString,iv);
    //EVP_DecryptUpdate(ctx,decryptPlaintext,&dlen,cipherString,cipherSize);
    //EVP_DecryptFinal_ex(ctx,decryptPlaintext+dlen,&dlen);

    //EVP_CIPHER_CTX_free(ctx);


    not_new = 1;
    fp = fopen(logpath, "a+");
    fgets(get_hash, 66, fp);
    fgets(get_salt, 33, fp);
    strncat(token, get_salt,32);
    token_len = strlen(token);
    ctx = EVP_MD_CTX_create(); 
    EVP_DigestInit_ex(ctx, EVP_sha256(), NULL); 
    EVP_DigestUpdate(ctx, token, token_len);
    EVP_DigestFinal_ex(ctx, hash, &len);
    for(i = 0; i < len; i++) {
      sprintf(curr, "%02x", hash[i]);
      strncat(hash_str, curr, 2);
    }
    if(strncmp(hash_str, get_hash,64) != 0) {
      printf("invalid");
      exit(255);
    }

  } else {
    // file does not exist, must create
    unsigned char hash_str[65] = "", curr[3] = "";
    fp = fopen(logpath, "a+");
    RAND_bytes(salt, 32);
    salt[32] = '\0';
    for(i = 0; i < 32; i++) {
      salt[i] = ((salt[i]%25)+97);
    }
    
    
    strncat(token, salt,32);
    token_len = strlen(token);
    ctx = EVP_MD_CTX_create(); 
    EVP_DigestInit_ex(ctx, EVP_sha256(), NULL); 
    EVP_DigestUpdate(ctx, token, token_len);
    EVP_DigestFinal_ex(ctx, hash, &len); 
    for(i = 0; i < len; i++) {
      sprintf(curr, "%02x", hash[i]);
      strncat(hash_str, curr, 2);
    }
    salt[32] = '\0';
    fprintf(fp, "%s\n%s\n", hash_str, salt);
    fclose(fp);

  }
  app_str = malloc(app_str_len);
  memset(app_str, '\0', app_str_len);
  if((employee == NULL && guest == NULL) || (employee != NULL && guest != NULL)) {
    
    printf("invalid");
    exit(255);
  } else if(employee != NULL && guest == NULL) {
    //what to do on employee
    for(i = 0; i < strlen(employee); i++) {
      if(!((employee[i] >= 'a' && employee[i] <= 'z') || (employee[i] >= 'A' && employee[i] <= 'Z'))) {
        printf("invalid");
        exit(255);
      }
    }
    strcat(app_str, "employee,");
    strcat(app_str, employee);
  } else {
    //what to do on guest
    for(i = 0; i < strlen(guest); i++) {
      if(!((guest[i] >= 'a' && guest[i] <= 'z') || (guest[i] >= 'A' && guest[i] <= 'Z'))) {
        printf("invalid");
        exit(255);
      }
    }
    strcat(app_str, "guest,");
    strcat(app_str, guest);
  }
  fp = fopen(logpath, "a+");
  if((arrival == 0 && departure == 0) || (arrival == 1 && departure == 1)) {
    printf("invalid dir");
    exit(255);
  } else if (arrival == 1 && departure == 0) {
    //what to do on arrival
    int check = 0, k = 0;
    char line[1025], *type = NULL, *name = NULL, *dir = NULL, *r = NULL, *t = NULL, t_check[12];
    FILE *fp2 = fopen(logpath, "r");
    strcpy(log, logpath);
    if(not_new) {
      while(fgets(line, 1024, fp2)) {
        if(strncmp(app_str, line, strlen(app_str)) == 0) {
          int num_of_commas = 0, comma_check, len1 = 1, len2 = 1, len3 = 1, len4 = 1, len5 = 1, j;
          if(type)
            free(type);
          if(name)
            free(name);
          if(dir)
            free(dir);
          if(room)
            free(r);
          if(t)
            free(t);  
          for(i = 0; i < strlen(line); i++) {
            if(line[i] == ',') {
              num_of_commas++;
            }
          }
          comma_check = num_of_commas;
          j = 0;
          for(i = 0; i < strlen(line); i++) {
            if(line[i] == ',') {
              comma_check--;
              if(comma_check < 0)
                break;
              continue;
            }
            if(num_of_commas - comma_check == 0)
              len1++;
            if(num_of_commas - comma_check == 1)
              len2++;
            if(num_of_commas - comma_check == 2)
              len3++;
            if(num_of_commas - comma_check == 3)
              len4++;
            if(num_of_commas - comma_check == 4)
              len5++;
          }
          if(len1 > 1)
            type = malloc(len1);
          if(len2 > 1)
            name = malloc(len2);
          if(len3 > 1)
            dir = malloc(len3);
          if(len4 > 1)
            r = malloc(len4);
          if(len5 > 1)
            t = malloc(len5);
          comma_check = num_of_commas;
          for(i = 0; i < strlen(line); i++) {
            if(line[i] == ',') {
              comma_check--;
              j = 0;
              if(comma_check < 0)
                break;
              continue;
            }
            if(num_of_commas - comma_check == 0)
              (type)[j++] = line[i];
            if(num_of_commas - comma_check == 1)
              (name)[j++] = line[i];
            if(num_of_commas - comma_check == 2)
              (dir)[j++] = line[i];
            if(num_of_commas - comma_check == 3)
              r[j++] = line[i];
            if(num_of_commas - comma_check == 4)
              (t)[j++] = line[i];
            
          }
          if(len1 > 1)
            type[len1] = '\0';
          if(len2 > 1)
            name[len2] = '\0';
          if(len3 > 1)
            dir[len3] = '\0';
          if(len4 > 1)
            r[len4] = '\0';
          if(len5 > 1)
            t[len5] = '\0';
        }
      }
      memset(t_check, '\0', 12);
      for(i = 0; i < strlen(line); i++) {
        if(line[i] == ',') {
          check++;
          continue;
        }
        if(check == 4){
          t_check[k++] = line[i];
        }
      } 
      if(atoi(t_check) >= timestamp) {
        printf("invalid");
        exit(255);
      }
    }
   // if((name == NULL && room != -1) || (dir != NULL && strcmp(dir, "depart") != 0 && atoi(r) != -1)) {
//printf("invalid rdA");
//exit(255);
   // }
    strcat(app_str, ",arrive");
    sprintf(room_str, ",%d", room);
    strcat(app_str, room_str);
    if(timestamp_bool) {
      sprintf(timestamp_str, ",%ld\n", timestamp);
      strcat(app_str, timestamp_str);
    } else {
      strcat(app_str, "\n");
    }
  } else {
    //what to do on departure
    int check = 0, k = 0;
    char line[1024], *type = NULL, *name = NULL, *dir = NULL, *r = NULL, *t = NULL, t_check[12];
    FILE *fp2 = fopen(logpath, "r");
    strcpy(log, logpath);
    if(not_new) {
      while(fgets(line, 1024, fp2)) {
        if(strncmp(app_str, line, strlen(app_str)) == 0) {
          int num_of_commas = 0, comma_check, len1 = 1, len2 = 1, len3 = 1, len4 = 1, len5 = 1, j;
          if(type)
            free(type);
          if(name)
            free(name);
          if(dir)
            free(dir);
          if(room)
            free(r);
          if(t)
            free(t);
          for(i = 0; i < strlen(line); i++) {
            if(line[i] == ',') {
              num_of_commas++;
            }
          }
          comma_check = num_of_commas;
          j = 0;
          for(i = 0; i < strlen(line); i++) {
            if(line[i] == ',') {
              comma_check--;
              if(comma_check < 0)
                break;
              continue;
            }
            if(num_of_commas - comma_check == 0)
              len1++;
            if(num_of_commas - comma_check == 1)
              len2++;
            if(num_of_commas - comma_check == 2)
              len3++;
            if(num_of_commas - comma_check == 3)
              len4++;
            if(num_of_commas - comma_check == 4)
              len5++;
          }
          if(len1 > 1)
            type = malloc(len1);
          if(len2 > 1)
            name = malloc(len2);
          if(len3 > 1)
            dir = malloc(len3);
          if(len4 > 1)
            r = malloc(len4);
          if(len5 > 1)
            t = malloc(len5);
          comma_check = num_of_commas;
          for(i = 0; i < strlen(line); i++) {
            if(line[i] == ',') {
              comma_check--;
              j = 0;
              if(comma_check < 0)
                break;
              continue;
            }
            if(num_of_commas - comma_check == 0)
              (type)[j++] = line[i];
            if(num_of_commas - comma_check == 1)
              (name)[j++] = line[i];
            if(num_of_commas - comma_check == 2)
              (dir)[j++] = line[i];
            if(num_of_commas - comma_check == 3)
              r[j++] = line[i];
            if(num_of_commas - comma_check == 4)
              (t)[j++] = line[i];
          }
          if(len1 > 1)
            type[len1] = '\0';
          if(len2 > 1)
            name[len2] = '\0';
          if(len3 > 1)
            dir[len3] = '\0';
          if(len4 > 1)
            r[len4] = '\0';
          if(len5 > 1)
            t[len5] = '\0';
        }
      }
      memset(t_check, '\0', 12);
      for(i = 0; i < strlen(line); i++) {
        if(line[i] == ',') {
          check++;
          continue;
        }
        if(check == 4){
          t_check[k++] = line[i];
        }
      } 
      if(atoi(t_check) >= timestamp) {
        printf("invalid");
        exit(255);
      }
      //if((dir && room == -1 && strcmp(dir, "depart") != 0 && atoi(r) != -1) || (dir && room != atoi(r) && strcmp(dir, "arrive") == 0)) {
     //   printf("invalid rdD");
     //   exit(255);
     // }
      strcat(app_str, ",depart");
      sprintf(room_str, ",%d", room);
      strcat(app_str, room_str);
      sprintf(timestamp_str, ",%ld\n", timestamp);
      strcat(app_str, timestamp_str);
    } else {
      printf("invalid");
      exit(255);
    }


  }

  if ((fp3 = fopen(log, "a")))
    fprintf(fp3, "%s", app_str);
  
  //////////////////////////////////////////////
  //FILE *filePlain;
  //int length, ciphertext_len;
  //unsigned char writeCipher[5096];
  //unsigned char * iv= (unsigned char *)"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

  //EVP_CIPHER_CTX *newctx;
  //newctx=EVP_CIPHER_CTX_new();

  //EVP_EncryptInit_ex(newctx,EVP_aes_128_cbc(),NULL,token,iv);
  //EVP_EncryptUpdate(newctx,writeCipher,&length, app_str,app_str_len);
  //ciphertext_len=length;
  //EVP_EncryptFinal_ex(newctx,writeCipher+length,&length);
  //ciphertext_len+=length;
  //EVP_CIPHER_CTX_free(newctx);

  //filePlain=fopen("new","a");
  //fwrite(writeCipher,ciphertext_len,1,filePlain);


  ///////////////////////////////////////////////

  return is_good;
}


int main(int argc, char *argv[]) {

  int result;
  result = parse_cmdline(argc, argv);




  return 0;
}
