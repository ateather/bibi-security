  #include <unistd.h>
  #include <sys/types.h>
  #include <sys/stat.h>
  #include <arpa/inet.h>
  #include <fcntl.h>
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <sys/queue.h>

  #include <openssl/ssl.h>
  #include <openssl/bio.h>
  #include <openssl/err.h>
  #include <openssl/rand.h>


  int contain(char *array,char * x){
    int i;
    int len=sizeof(array)/sizeof(array[0]);
    for(i=0; i < len; i++){
      if (strcmp(array[i],x)==0){
        return 1;
      }
    }
    return 0;
  }

  static int myCompare(const void *a,const void* b){
  	return strcmp(*(const char **)a, *(const char **) b);
  }


  static int intCompare(const void *a,const void* b){
    return atoi(*(const char **)a) > atoi(*(const char **) b);
  }

  void intsort( const char * arr[], int n){
    qsort(arr,n,sizeof(const char *),intCompare);
  }

  void sort( const char * arr[], int n){
  	qsort(arr,n,sizeof(const char *),myCompare);
  }


  int main(int argc, char *argv[]) {
    int   opt;
    char  *logpath = NULL;
    unsigned int len, token_len;
    unsigned char *token = NULL;
    FILE *fp;
    EVP_MD_CTX *ctx; 
    unsigned char salt[33],storedHash[128],hash[128];
    int sflag=0, rflag=0, gflag=0, eflag=0;
    char * employeeStored;
    char * guestStored;
    char time[64];
    unsigned char * iv= (unsigned char *)"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
    int commaFlag=0;





    while ((opt = getopt(argc, argv, "K:PSRE:G:VT")) != -1) {
      switch(opt) {
        case 'T':
          printf("unimplemented");
          exit(255);
          break;

        case 'K':
          token = malloc(strlen(optarg) + 33);
          strcpy(token, optarg);
          token[strlen(optarg)] = '\0';
          break;

        case 'S':
          //prints gallery state to stdout
          sflag=1;
          break;

        case 'R':
          //Room number
          rflag=1;

          break;

        case 'E':
          //Employee
          employeeStored=malloc(strlen(optarg));
          strcpy(employeeStored,optarg);
          employeeStored[strlen(optarg)]='\0';
          eflag=1;
          break;

        case 'G':
          guestStored=malloc(strlen(optarg));
          strcpy(guestStored,optarg);
          guestStored[strlen(optarg)]='\0';
          gflag=1;
          //Guest Name
          break;
      }
    }


    if(optind < argc) {
      logpath = argv[optind];
    }

    

    if(access(logpath,F_OK)==0){

      unsigned char get_hash[67], get_salt[34], hash_str[65] = "", curr[3] = "";
      unsigned int a;

    
              //int len;
              //unsigned char * cipherString=malloc(1024);
              //unsigned char * realString=malloc(1024);

              //fgets(cipherString,sizeof(cipherString),fp);

              //EVP_CIPHER_CTX *ctx;
              //ctx=EVP_CIPHER_CTX_new();
              //EVP_DecryptInit_ex(ctx,EVP_aes_128_cbc(),NULL,cipherString,iv);
              //EVP_DecryptUpdate(ctx,realString,&len,cipherString,sizeof(cipherString));
              //EVP_DecryptFinal_ex(ctx,realString+len,&len);
              //EVP_CIPHER_CTX_free(ctx);

              //free(cipherString);
              

      fp = fopen(logpath, "r");
      fgets(get_hash, 66, fp);
      fgets(get_salt, 33, fp);
      strncat(token, get_salt,32);
      token_len = strlen(token);
      ctx = EVP_MD_CTX_create(); 
      EVP_DigestInit_ex(ctx, EVP_sha256(), NULL); 
      EVP_DigestUpdate(ctx, token, token_len);
      EVP_DigestFinal_ex(ctx, hash, &len);
      for(a = 0; a < len; a++) {
        sprintf(curr, "%02x", hash[a]);
        strncat(hash_str, curr, 2);
      }

      if(strncmp(hash_str, get_hash,64) == 0){
        //IF BOTH OF THE HASHES MATCH//
        if(sflag){
          char * roomNumber[50];
          char * employeeRoom[100];
          char * guestRoom[100];
          char * employeeArray[50];
          char * guestArray[50];
          int i=0,j=0,k=0;
          int guestCounter=0, employeeCounter=0, roomCounter=0;
          int guestDup=0,employeeDup=0;
          int guestCommaFlag=0,employeeCommaFlag=0;
          int guestRoomIndex,employeeRoomIndex;
          int roomNumberIndex=0,roomDup=0;
          

          
          char realString[64];
          while(fgets(realString,64,fp)){
            
            
            

            
              char * pch;
              pch=strtok(realString,",");

              if(strcmp(pch,"guest")==0){
                  pch=strtok(NULL,",");
                  guestDup=0;
                  for(i=0;i<guestCounter; i++){
                  	if(strcmp(pch,guestArray[i])==0){
                  		guestDup=1;
                      guestRoomIndex=i*2;
                      pch=strtok(NULL,",");
                      if(strcmp(pch,"arrive")==0){
                        pch=strtok(NULL,",");
                        for(i=0;i<roomNumberIndex;i++){      
                          if(strcmp(roomNumber[i],pch)==0){
                            roomDup=1;
                          }
                        }
                      if(roomDup==0){
                        roomNumber[roomNumberIndex]=malloc(sizeof(pch));
                        strncpy(roomNumber[roomNumberIndex],pch,strlen(pch));
                        roomNumberIndex++;
                      }
                      roomDup=0;
                        free(guestRoom[guestRoomIndex+1]);
                        guestRoom[guestRoomIndex+1]=malloc(sizeof(pch));
                        strncpy(guestRoom[guestRoomIndex+1],pch,sizeof(pch));
                      }else if(strcmp(pch,"depart")==0){
                        pch=strtok(NULL,",");
                        for(i=0;i<roomNumberIndex;i++){      
                          if(strcmp(roomNumber[i],pch)==0){
                            roomDup=1;
                          }
                        }
                      if(roomDup==0){
                        roomNumber[roomNumberIndex]=malloc(sizeof(pch));
                        strncpy(roomNumber[roomNumberIndex],pch,strlen(pch));
                        roomNumberIndex++;
                      }
                        roomDup=0;
                        free(guestRoom[guestRoomIndex+1]);
                        if(strcmp(pch,"-1")==0){
                          guestRoom[guestRoomIndex+1]=malloc(sizeof(""));
                          strncpy(guestRoom[guestRoomIndex+1],"",sizeof(""));
                        }else{
                          guestRoom[guestRoomIndex+1]=malloc(sizeof("-1"));
                          strncpy(guestRoom[guestRoomIndex+1],"-1",sizeof("-1"));

                        }
                      }
                  	}
                  }
                  if(guestDup==0){
                 		guestArray[guestCounter]=malloc(sizeof(pch));
                 		strncpy(guestArray[guestCounter++],pch,strlen(pch));
                    char * name=malloc(sizeof(pch));
                    strncpy(name,pch,strlen(pch));
                    guestRoomIndex=(guestCounter-1)*2;
                    pch=strtok(NULL,",");
                    if(strcmp(pch,"arrive")==0){
                      pch=strtok(NULL,",");
                      for(i=0;i<roomNumberIndex;i++){      
                          if(strcmp(roomNumber[i],pch)==0){
                            roomDup=1;
                          }
                      }
                      if(roomDup==0){
                        roomNumber[roomNumberIndex]=malloc(sizeof(pch));
                        strncpy(roomNumber[roomNumberIndex],pch,strlen(pch));
                        roomNumberIndex++;
                      }
                      roomDup=0;
                      guestRoom[guestRoomIndex]=malloc(sizeof(name));
                      guestRoom[guestRoomIndex+1]=malloc(sizeof(pch));
                      strncpy(guestRoom[guestRoomIndex],name,strlen(name));
                      strncpy(guestRoom[guestRoomIndex+1],pch,strlen(pch));
                      free(name);
                    }

                    /*else if(strcmp(pch,"depart")==0){
                      pch=strtok(NULL,",");
                      for(i=0;i<roomNumberIndex;i++){      
                          if(strcmp(roomNumber[i],pch)==0){
                            roomDup=1;
                          }
                      }
                      if(roomDup==0){
                        roomNumber[roomNumberIndex]=malloc(sizeof(pch));
                        strncpy(roomNumber[roomNumberIndex],pch,strlen(pch));
                        roomNumberIndex++;
                      }
                      roomDup=0;
                      guestRoom[guestRoomIndex]=malloc(sizeof(name));
                      guestRoom[guestRoomIndex+1]=malloc(sizeof(""));
                      strncpy(guestRoom[guestRoomIndex],name,strlen(name));
                      strncpy(guestRoom[guestRoomIndex+1],"",strlen(""));
                      free(name);
                    }*/
                 	}

              } else if(strcmp(pch,"employee")==0){
                 pch=strtok(NULL,",");
                  employeeDup=0;
                  for(i=0;i<employeeCounter; i++){
                    if(strcmp(pch,employeeArray[i])==0){
                      employeeDup=1;
                      employeeRoomIndex=i*2;
                      pch=strtok(NULL,",");
                      if(strcmp(pch,"arrive")==0){
                        pch=strtok(NULL,",");
                        for(i=0;i<roomNumberIndex;i++){      
                          if(strcmp(roomNumber[i],pch)==0){
                            roomDup=1;
                          }
                      }

                      if(roomDup==0){
                        roomNumber[roomNumberIndex]=malloc(sizeof(pch));
                        strncpy(roomNumber[roomNumberIndex],pch,strlen(pch));
                        
                        roomNumberIndex++;
                      }
                      roomDup=0;
                        free(employeeRoom[employeeRoomIndex+1]);
                        employeeRoom[employeeRoomIndex+1]=malloc(sizeof(pch));
                        strncpy(employeeRoom[employeeRoomIndex+1],pch,sizeof(pch));
                      }else if(strcmp(pch,"depart")==0){
                        pch=strtok(NULL,",");
                        for(i=0;i<roomNumberIndex;i++){      
                          if(strcmp(roomNumber[i],pch)==0){
                            roomDup=1;
                          }
                      }

                      if(roomDup==0){
                        roomNumber[roomNumberIndex]=malloc(sizeof(pch));
                        strncpy(roomNumber[roomNumberIndex],pch,strlen(pch));
                        
                        roomNumberIndex++;
                      }
                        roomDup=0;
                        free(employeeRoom[employeeRoomIndex+1]);
                        if(strcmp(pch,"-1")==0){  
                          employeeRoom[employeeRoomIndex+1]=malloc(sizeof(""));
                          strncpy(employeeRoom[employeeRoomIndex+1],"",sizeof(""));
                        }else{
                          employeeRoom[employeeRoomIndex+1]=malloc(sizeof("-1"));
                          strncpy(employeeRoom[employeeRoomIndex+1],"-1",sizeof("-1"));
                        }
                      }
                    }
                  }
                  if(employeeDup==0){
                    employeeArray[employeeCounter]=malloc(sizeof(pch));
                    strncpy(employeeArray[employeeCounter++],pch,strlen(pch));

                    char * name=malloc(sizeof(pch));
                    strncpy(name,pch,strlen(pch));
                    employeeRoomIndex=(employeeCounter-1)*2;
                    pch=strtok(NULL,",");
                    if(strcmp(pch,"arrive")==0){
                      pch=strtok(NULL,",");
                      for(i=0;i<roomNumberIndex;i++){      
                          if(strcmp(roomNumber[i],pch)==0){
                            roomDup=1;
                          }
                      }

                      if(roomDup==0){
                        roomNumber[roomNumberIndex]=malloc(sizeof(pch));
                        strncpy(roomNumber[roomNumberIndex],pch,strlen(pch));
                        
                        roomNumberIndex++;
                      }
                      roomDup=0;
                      employeeRoom[employeeRoomIndex]=malloc(sizeof(name));
                      employeeRoom[employeeRoomIndex+1]=malloc(sizeof(pch));
                      strncpy(employeeRoom[employeeRoomIndex],name,strlen(name));
                      strncpy(employeeRoom[employeeRoomIndex+1],pch,strlen(pch));
                      free(name);
                    }
                    /*else if(strcmp(pch,"depart")==0){
                      pch=strtok(NULL,",");
                      for(i=0;i<roomNumberIndex;i++){      
                          if(strcmp(roomNumber[i],pch)==0){
                            roomDup=1;
                          }
                      }

                      if(roomDup==0){
                        roomNumber[roomNumberIndex]=malloc(sizeof(pch));
                        strncpy(roomNumber[roomNumberIndex],pch,strlen(pch));
                        
                        roomNumberIndex++;
                      }
                      roomDup=0;
                      employeeRoom[employeeRoomIndex]=malloc(sizeof(name));
                      employeeRoom[employeeRoomIndex+1]=malloc(sizeof(""));
                      strncpy(employeeRoom[employeeRoomIndex],name,strlen(name));
                      strncpy(employeeRoom[employeeRoomIndex+1],"",strlen(""));
                      free(name);
                    }*/
                  }
              }	
             
              	





      	}
      	sort(guestArray,guestCounter);
      	sort(employeeArray,employeeCounter);
        intsort(roomNumber,roomNumberIndex);

      	char * employeePrint[50];
        int employeePrintCounter=0;


      	for(i=0;i<employeeCounter;i++){
          for(j=0;j<employeeCounter*2-1;j++){
            if(strcmp(employeeArray[i],employeeRoom[j])==0){
              if(strcmp(employeeRoom[j+1],"")!=0){
                employeePrint[employeePrintCounter]=malloc(sizeof(employeeRoom[j]));
                strncpy(employeePrint[employeePrintCounter],employeeRoom[j],strlen(employeeRoom[j]));
                employeePrintCounter++;
              }
            }
          }
        }

        int newlineFlag=0;
      	
        if(employeePrintCounter==1){
          printf("%s",employeePrint[employeePrintCounter-1]);
          newlineFlag=1;
        }else if(employeePrintCounter>1){

          for(i=0;i<employeePrintCounter-1;i++){
            printf("%s,",employeePrint[i]);
            newlineFlag=1;
          }
          printf("%s",employeePrint[i]);
        }

       

///////////////
      	char * guestPrint[50];
        int guestPrintCounter=0;


        for(i=0;i<guestCounter;i++){
          for(j=0;j<guestCounter*2-1;j++){
            if(strcmp(guestArray[i],guestRoom[j])==0){
              if(strcmp(guestRoom[j+1],"")!=0){
                guestPrint[guestPrintCounter]=malloc(sizeof(guestRoom[j]));
                strncpy(guestPrint[guestPrintCounter],guestRoom[j],strlen(guestRoom[j]));
                guestPrintCounter++;
              }
            }
          }
        }
        
        if(guestPrintCounter==1){
          if(newlineFlag){
            printf("\n");
          }
          printf("%s",guestPrint[guestPrintCounter-1]);
          newlineFlag=1;
        }else if(guestPrintCounter>1){
          if(newlineFlag){
            printf("\n");
          }
          for(i=0;i<guestPrintCounter-1;i++){
            printf("%s,",guestPrint[i]);
          }
          printf("%s",guestPrint[i]);
          newlineFlag=1;
        } 
///////////////////////
        
        for(i=1;i<roomNumberIndex;i++){
          char * logArray [100];
          int peopleCounter=0;
          
          for(j=0;j<employeeCounter*2;j++){
            if(strcmp(roomNumber[i],employeeRoom[j])==0){
              logArray[peopleCounter]=malloc(sizeof(employeeRoom[j-1]));           
              strncpy(logArray[peopleCounter],employeeRoom[j-1],strlen(employeeRoom[j-1]));
              peopleCounter++;
            }
          }
          for(j=0;j<guestCounter*2;j++){
            if(strcmp(roomNumber[i],guestRoom[j])==0){
              logArray[peopleCounter]=malloc(sizeof(guestRoom[j-1]));           
              strncpy(logArray[peopleCounter],guestRoom[j-1],strlen(guestRoom[j-1]));
              peopleCounter++;
            }
          }
          sort(logArray,peopleCounter);

          if( peopleCounter==1){
            printf("\n%s: ",roomNumber[i]);
            printf("%s",logArray[0]);
          }else if (peopleCounter>1){
            printf("\n%s: ",roomNumber[i]);
            for(k=0;k<peopleCounter-1;k++){
              printf("%s,",logArray[k]);
            }
            printf("%s",logArray[k]);
          }
          
      

        }



        }else if (gflag && rflag){
          char *currentRoom="-10000";
          char realString[64];
           while(fgets(realString,64,fp)){
            
            
            

            
              char * pch;
              pch=strtok(realString,",");
              if(strcmp(pch,"guest")==0){
                  pch=strtok(NULL,",");
                  if(strcmp(pch,guestStored)==0){
                      pch=strtok(NULL,",");
                      pch=strtok(NULL,",");  
                      if(strcmp(currentRoom,pch)!=0 && strcmp(pch,"-1")!=0){
                        if(commaFlag){
                          printf(",");
                        }
                        commaFlag=1;
                        currentRoom=malloc(sizeof(pch));
                        strncpy(currentRoom,pch,strlen(pch));
                        printf("%s\n", pch);
                      } 
                  }
              }
          }





        }else if (eflag && rflag){

          char *currentRoom="-10000";
          char realString[64];
           while(fgets(realString,64,fp)){
            
            
            

              char * pch;
              pch=strtok(realString,",");

              
              if(strcmp(pch,"employee")==0){
                  pch=strtok(NULL,",");
                  if(strcmp(pch,employeeStored)==0){
                      pch=strtok(NULL,",");
                      pch=strtok(NULL,",");
                      if(strcmp(currentRoom,pch)!=0 && strcmp(pch,"-1")!=0){
                        if(commaFlag){
                          printf(",");
                        }
                        commaFlag=1;
                        currentRoom=malloc(sizeof(pch));
                        strncpy(currentRoom,pch,strlen(pch));
                        printf("%s", pch);
                      } 
                  }
              }
          }
           //////////////////////////////////////////////
  //FILE *filePlain;
  //int length, ciphertext_len;
  //unsigned char writeCipher[5096];
  //unsigned char * iv= (unsigned char *)"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

  //EVP_CIPHER_CTX *newctx;
  //newctx=EVP_CIPHER_CTX_new();

  //EVP_EncryptInit_ex(newctx,EVP_aes_128_cbc(),NULL,token,iv);
  //EVP_EncryptUpdate(newctx,writeCipher,&length, app_str,app_str_len);
  //ciphertext_len=length;
  //EVP_EncryptFinal_ex(newctx,writeCipher+length,&length);
  //ciphertext_len+=length;
  //EVP_CIPHER_CTX_free(newctx);

  //filePlain=fopen("new","a");
  //fwrite(writeCipher,ciphertext_len,1,filePlain);


  ///////////////////////////////////////////////



        } else{
          printf("invalid");
          exit(255);
        }
      }else{
        printf("invalid");
        exit(255);
      }


    }else{
      printf("invalid");
      exit(255);
    }
    return 0;
  }
