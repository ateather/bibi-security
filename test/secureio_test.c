#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../build/secureio.h"

/*
 * function:

 * will decrypt file using key and save plaintext in outtxt
 * memory for outtxt is allocated in the fuction, so free outtxt when not needed
 * return vale: length of the plaintext
   int read_file(char* filename, char* key, char** outtxt);

 * will encrypt txt using key write ciphertext to the file
 * return value: size of output file;
   int write_file(char* filename, char* key, char* txt, int txt_len);
 */

int main(int argc, char** argv){
  char filename[10] = "f1.log";
  char key1[32] = "131313";
  char key2[32] = "090909";
  char msg[128] = "\x01\x23\x45\x67\x89\xAB\xCD\x0F";
  unsigned int fsize = write_file(filename, key1, msg, 8);
  printf("write finished, size: %d\n", fsize);
  char* plaintext = NULL;
  read_file(filename, key1, &plaintext);
  if(strcmp(plaintext, msg) == 0)
    printf("Test 1 Passed: message matched!\n");
  else
    printf("Test 1 Failed: \nExpected: %s \nActual: %s\n\n", msg, plaintext);
  free(plaintext);

  printf("Test 2 Pass if integrity voilation and exited immidiately\n");
  read_file(filename, key2, &plaintext);
  printf("Test 2 Failed: integrity failure not detected!\n");
  free(plaintext);
  return 0;
}
