#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "secureio.h"

#define NAME_MAX_LENGTH 128

typedef struct {
  char type; // G for Guest, E for Employee
  char name[NAME_MAX_LENGTH];
} Person;

typedef struct{
  unsigned int time;
  Person person;
  char action;
  int room;
} Record;

typedef struct{
  Person person;
  unsigned int num_records;
  unsigned int* indices;
} Entry;

typedef struct{
  unsigned int size;
  Entry* entries;
} HashTable;

typedef struct{
  HashTable table;
  unsigned int record_count;
  Record* record_list;
} Log;

typedef struct{
  int room_id;
  unsigned int num_person;
  Person* person_list;
} Room;

Log* new_log(){
  Log* log = malloc(sizeof(Log));
  log -> table.size = 0;
  log -> table.entries = malloc(log -> table.size * sizeof(Entry));
  log -> record_count = 0;
  log -> record_list = malloc(log -> record_count * sizeof(Record));
  return log;
}

void free_log(Log* log){
  int i;
  for(i = 0; i < log -> table.size; i++)
    free(log -> table.entries[i].indices);
  free(log -> table.entries);
  free(log -> record_list);
  free(log);
}

unsigned int entry2binary(Entry entry, char** out_binary){
  unsigned int bin_len = sizeof(Entry) + entry.num_records * sizeof(unsigned int);

  *out_binary = malloc(bin_len);
  memcpy(*out_binary, &entry, sizeof(Entry));
  memcpy(*out_binary + sizeof(Entry), entry.indices, entry.num_records * sizeof(unsigned int));

  return bin_len;
}

char* binary2entry(char* binary, Entry* out_entry){
  memcpy(out_entry, binary, sizeof(Entry));
  unsigned int* indices = malloc(out_entry -> num_records * sizeof(unsigned int));
  memcpy(indices, binary + sizeof(Entry), out_entry -> num_records * sizeof(unsigned int));
  out_entry -> indices = indices;
  return binary + sizeof(Entry) + out_entry -> num_records * sizeof(unsigned int);
}

unsigned int table2binary(HashTable table, char** out_binary){
  unsigned int bin_len = sizeof(HashTable);
  *out_binary = malloc(bin_len);
  memcpy(*out_binary, &table, sizeof(HashTable));
  int i;
  for(i = 0; i < table.size; i++){
    char* entry_bin = NULL;
    unsigned int entry_bin_len = entry2binary(table.entries[i], &entry_bin);
    *out_binary = realloc(*out_binary, bin_len + entry_bin_len);
    // printf("writing entry %d offset 0x%x\n", i, bin_len);
    memcpy(*out_binary + bin_len, entry_bin, entry_bin_len);
    bin_len += entry_bin_len;
    free(entry_bin);
  }

  // printf("wrote table at %p\n", *out_binary);
  return bin_len;
}

char* binary2table(char* binary, HashTable* out_table){
  // printf("reading table at %p\n", binary);
  memcpy(out_table, binary, sizeof(HashTable));
  char* bin_ptr = binary + sizeof(HashTable);
  out_table -> entries = malloc(out_table -> size * sizeof(Entry));
  int i;
  for(i = 0; i < out_table -> size; i++){
    // printf("reading entry %d offset 0x%x\n", i, bin_ptr - binary);
    bin_ptr = binary2entry(bin_ptr, &(out_table -> entries[i]));
  }
  return bin_ptr;
}

Log* load_log(char* filename, char* key){//, HashTable* out_table, Record** record_list){
  char* data = NULL;
  char* bin_ptr = NULL;
  if(read_file(filename, key, &data) == 0)
    return NULL;
  Log* log = new_log();
  bin_ptr = binary2table(data, &(log -> table));
  unsigned int* record_count = (unsigned int*) bin_ptr;
  log -> record_count = *record_count;
  log -> record_list = realloc(log -> record_list, *record_count * sizeof(Record));
  memcpy(log -> record_list, record_count + 1, *record_count * sizeof(Record));
  free(data);
  return log;
}

unsigned int write_log(char* filename, char* key, Log* log){//HashTable table, Record* record_list, unsigned int record_count){
  char* data = NULL;
  char* table_bin = NULL;
  unsigned int table_bin_len = table2binary(log -> table, &table_bin);
  unsigned int data_len = table_bin_len + sizeof(unsigned int) + log -> record_count * sizeof(Record);

  data = malloc(data_len);
  memcpy(data, table_bin, table_bin_len);
  memcpy(data + table_bin_len, &(log -> record_count), sizeof(unsigned int));
  memcpy(data + table_bin_len + sizeof(unsigned int), log -> record_list, log -> record_count * sizeof(Record));

  unsigned int fsize = write_file(filename, key, data, data_len);

  free(table_bin);
  free(data);

  return fsize;
}

int person_cmp(Person p1, Person p2){
  if(p1.type != p2.type) return -1;
  return strcmp(p1.name, p2.name);
}

Entry* get_entry_ptr(Log* log, Person p){
  int i;
  for(i = 0; i < log -> table.size; i++)
    if(person_cmp(log -> table.entries[i].person, p) == 0)
      return &(log -> table.entries[i]);
  return NULL;
}

Record* get_last_record_of_person(Log* log, Person person){
  Entry* entry = get_entry_ptr(log, person);
  if(entry)
    return &(log -> record_list[entry -> indices[entry -> num_records - 1]]);
  else
    return NULL;
}

unsigned int get_last_record_time(Log* log){
  if(log -> record_count == 0) return 0;
  return log -> record_list[log -> record_count - 1].time;
}

Room* find_room(Room* rooms, unsigned int room_count, int room_id){
  int i;
  for(i = 0; i < room_count; i++)
    if(rooms[i].room_id == room_id)
      return &(rooms[i]);
  return NULL;
}

// Returns number of rooms occupied
// output list of room information to <rooms>
// remember to use free_rooms(<rooms>, <num_rooms>) when not needed
unsigned int get_all_room_status(Log* log, Room** rooms){
  unsigned int room_count = 0;
  *rooms = malloc(room_count * sizeof(Room));
  int i;
  for(i = 0; i < log -> table.size; i++){
    Record* record = get_last_record_of_person(log, log -> table.entries[i].person);
    int room_id;
    Room* room = NULL;
    if(record -> action != 'L' || record -> room != -1){
      if(record -> action == 'A')
        room_id = record -> room;
      else
        room_id = -1;
      if(!(room = find_room(*rooms, room_count, room_id))){
        *rooms = realloc(*rooms, ++room_count * sizeof(Room));
        room = &((*rooms)[room_count - 1]);
        room -> room_id = room_id;
        room -> num_person = 0;
        room -> person_list = malloc(room -> num_person * sizeof(Person));
      }
      room -> person_list = realloc(room -> person_list, ++(room -> num_person) * sizeof(Person));
      room -> person_list[room -> num_person - 1] = log -> table.entries[i].person;
    }
  }
  return room_count;
}

void free_rooms(Room* rooms, unsigned int num_rooms){
  int i;
  for(i = 0; i < num_rooms; i++){
    free(rooms[i].person_list);
  }
  free(rooms);
}

int append_record(Log* log, Record new_record){
  // Note: This method does not validate the current state of the person
  // all it does is put the new_record into the list and add it to the table
  log -> record_list = realloc(log -> record_list, ++(log -> record_count) * sizeof(Record));
  log -> record_list[log -> record_count - 1] = new_record;
  Entry* entry = NULL;
  if(!(entry = get_entry_ptr(log, new_record.person))){
    log -> table.entries = realloc(log -> table.entries, ++(log -> table.size) * sizeof(Entry));
    entry = &(log -> table.entries[log -> table.size - 1]);
    entry -> person = new_record.person;
    entry -> num_records = 0;
    entry -> indices = malloc(entry -> num_records * sizeof(unsigned int));
  }
  entry -> indices = realloc(entry -> indices, ++(entry -> num_records) * sizeof(unsigned int));
  entry -> indices[entry -> num_records - 1] = log -> record_count - 1;
  return log -> record_count;
}
