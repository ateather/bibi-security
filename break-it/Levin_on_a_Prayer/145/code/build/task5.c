#include <stdio.h>    /* for printf() */
#include <stdlib.h>   /* for EXIT_SUCCESS */
#include <string.h>
#include <openssl/evp.h>

int main(int argc, char * argv[]) {
  FILE *filePlain;
  FILE *fileCipher;
  FILE *fileCipher1;
  FILE *fileWords;
  FILE *fileKey;
  FILE *fileOutput;
  int flag=0;
  int plaintextLength=0;
  int cipherLength=0;
  unsigned char plaintext[32];
  unsigned char wordsString[17];
  unsigned char cipherString[128];
  unsigned char keyString[128];
  int cipherSize=0;
  unsigned char writeCipher[128];
  unsigned char * iv= (unsigned char *)"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
  wordsString[0]='\0';

  fileKey=fopen(argv[2],"r");
  if(!fileKey){
    printf("ERROR\n");
    exit(0);
  }
  fseek(fileKey,0L,SEEK_END);
  int keySize=ftell(fileKey);
  fseek(fileKey,0L,SEEK_SET);

  fread(keyString,keySize,1,fileKey);




  
  if(strcmp(argv[1],"read")==0){

    int dlen;
    unsigned char decryptPlaintext[32];
    decryptPlaintext[32]='\0';
    fileCipher = fopen(argv[3],"rb");
    if(!fileCipher){
      printf("ERROR\n");
      exit(0);
    }

    fseek(fileCipher,0L,SEEK_END);
    cipherSize=ftell(fileCipher);
    fseek(fileCipher,0L,SEEK_SET);

  
    if(fileCipher){
  	 fread(cipherString,sizeof(cipherString),1,fileCipher);
    }

    EVP_CIPHER_CTX *ctx;
    ctx=EVP_CIPHER_CTX_new();
    EVP_DecryptInit_ex(ctx,EVP_aes_128_cbc(),NULL,keyString,iv);
    EVP_DecryptUpdate(ctx,decryptPlaintext,&dlen,cipherString,cipherSize);
    EVP_DecryptFinal_ex(ctx,decryptPlaintext+dlen,&dlen);

    EVP_CIPHER_CTX_free(ctx);
    if(decryptPlaintext[0]==' '){
      printf("%s\n",decryptPlaintext+1);
    }else{

      printf("INVALID");
    }




  }

  if(strcmp(argv[1],"write")==0){
    int len, ciphertext_len;
    
    filePlain= fopen(argv[3],"r");
    if(filePlain){
      fgets(plaintext+1,sizeof(plaintext),filePlain);
    }
    plaintext[sizeof(plaintext)]='\0';

    
    plaintext[0]=' ';
    EVP_CIPHER_CTX *ctx;
    ctx=EVP_CIPHER_CTX_new();

    EVP_EncryptInit_ex(ctx,EVP_aes_128_cbc(),NULL,keyString,iv);
    EVP_EncryptUpdate(ctx,writeCipher,&len, plaintext,strlen(plaintext));
    ciphertext_len=len;
    EVP_EncryptFinal_ex(ctx,writeCipher+len,&len);
    ciphertext_len+=len;
    EVP_CIPHER_CTX_free(ctx);

    fileOutput=fopen("cipher.bin","w");
    if(fileOutput){
      fwrite(writeCipher,ciphertext_len,1,fileOutput);
    }
    
  }  

  return 0;
}


