#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "common.h"

#define FLAG_S 1
#define FLAG_R 2
#define FLAG_I 4
#define FLAG_T 8

Person new_person(char type, char* name);
int person_void_cmp(const void* person1, const void* person2);
int room_cmp(const void* room1, const void* room2);
int record_cmp(const void* record1, const void* record2);

int main(int argc, char** argv){
  char* token = NULL;
  int num_guest = 0;
  Person* guest_list = malloc(num_guest * sizeof(Person));
  int num_employee = 0;
  Person* employee_list = malloc(num_employee * sizeof(Person));
  int flag = 0;
  char* filepath = NULL;
  Person* person = NULL;
  Room* rooms = NULL;
  Record* record = NULL;
  unsigned int record_count = 0;
  Record* records = NULL;
  Entry* entry = NULL;
  Log* log = NULL;
  int is_first;

  int i, j;

  for(i = 1; i < argc; i++){
    // printf("%s\n", argv[i]);
    if(strcmp(argv[i], "-K") == 0){
      // Key
      if(++i >= argc) invalid();
      token = argv[i];
      // printf("Your token is: %s\n", token);
    }else if(strcmp(argv[i], "-R") == 0){
      // Room
      flag |= FLAG_R;
    }else if(strcmp(argv[i], "-E") == 0){
      if(++i >= argc) invalid();
      employee_list = realloc(employee_list, ++num_employee * sizeof(Person));
      employee_list[num_employee - 1] = new_person('E', argv[i]);
    }else if(strcmp(argv[i], "-G") == 0){
      if(++i >= argc) invalid();
      guest_list = realloc(guest_list, ++num_guest * sizeof(Person));
      guest_list[num_guest - 1] = new_person('G', argv[i]);
    }else if(strcmp(argv[i], "-S") == 0){
      // State
      flag |= FLAG_S;
    }else if(strcmp(argv[i], "-I") == 0){
      // Room ID
      flag |= FLAG_I;
    }else if(strcmp(argv[i], "-T") == 0){
      // Time
      flag |= FLAG_T;
    }else{
      filepath = argv[i];
    }
  }

  // printf("Parsing Done.\n");

  // token consists of an arbitrary sized string of alphanumeric characters
  if(!token || !isalnumstr(token)) invalid();
  // printf("Token validated. \n");
  // printf("Your token is: %s\n", token);
  if(!filepath || !is_valid_filepath(filepath)) invalid();
  // printf("Filepath validated.\n");
  // TODO: What to do when file failed to open?
  if(!(log = load_log(filepath, token))) invalid();
  // printf("Log loaded.\n");

  switch (flag) {
    case FLAG_R:
      if(num_guest && !num_employee)
        person = &(guest_list[num_guest - 1]);
      else if(!num_guest && num_employee)
        person = &(employee_list[num_employee - 1]);
      else
        invalid();
      Entry* e = get_entry_ptr(log, *person);
      if(!e) return 0;
      Record* r = NULL;
      is_first = 1;
      for(i = 0; i < e -> num_records; i++){
        r = &(log -> record_list[e -> indices[i]]);
        if(r -> action == 'A' && r -> room != GALLERYID){
          if(!is_first)
            printf(",");
          printf("%d", r -> room);
          is_first = 0;
        }
      }
      if(!is_first)
        printf("\n");
      break;
    case FLAG_S:
      // TODO: Get all person in all room
      num_employee = 0;
      num_guest = 0;
      unsigned int num_rooms = get_all_room_status(log, &rooms);
      for(i = 0; i < num_rooms; i++){
        for(j = 0; j < rooms[i].num_person; j++){
          if(rooms[i].person_list[j].type == 'E'){
            employee_list = realloc(employee_list, ++num_employee * sizeof(Person));
            employee_list[num_employee - 1] = rooms[i].person_list[j];
          }else{
            guest_list = realloc(guest_list, ++num_guest * sizeof(Person));
            guest_list[num_guest - 1] = rooms[i].person_list[j];
          }
        }
      }
      qsort(employee_list, num_employee, sizeof(Person), person_void_cmp);
      qsort(guest_list, num_guest, sizeof(Person), person_void_cmp);
      is_first = 1;
      for(i = 0; i < num_employee; i++){
        if(!is_first)
          printf(",");
        printf("%s", employee_list[i].name);
        is_first = 0;
      }
      printf("\n");
      is_first = 1;
      for(i = 0; i < num_guest; i++){
        if(!is_first)
          printf(",");
        printf("%s", guest_list[i].name);
        is_first = 0;
      }
      printf("\n");
      qsort(rooms, num_rooms, sizeof(Room), room_cmp);
      for(i = 0; i < num_rooms; i++){
        if(rooms[i].room_id != GALLERYID){
          printf("%d: ", rooms[i].room_id);
          qsort(rooms[i].person_list, rooms[i].num_person, sizeof(Person), person_void_cmp);
          is_first = 1;
          for(j = 0; j < rooms[i].num_person; j++){
            if(!is_first)
              printf(",");
            printf("%s", rooms[i].person_list[j].name);
            is_first = 0;
          }
          printf("\n");
        }
      }
      break;
    case FLAG_T:
      if(num_guest && !num_employee)
        person = &(guest_list[num_guest - 1]);
      else if(!num_guest && num_employee)
        person = &(employee_list[num_employee - 1]);
      else
        invalid();
      if((entry = get_entry_ptr(log, *person))){
        unsigned int total_time = 0;
        unsigned int last_visit_time = 0;
        for(i = 0; i < entry -> num_records; i++){
          record = &(log -> record_list[entry -> indices[i]]);
          if(record -> room == GALLERYID){
            switch (record -> action) {
              case 'A':
                last_visit_time = record -> time;
                break;
              case 'L':
                // TODO: check for integer overflow
                total_time += record -> time - last_visit_time;
                last_visit_time = 0;
            }
          }
        }
        if(last_visit_time > 0)
          total_time += get_last_record_time(log) - last_visit_time;
        printf("%u\n", total_time);
      }
      break;
    case FLAG_I:
      records = malloc(record_count * sizeof(Record));
      for(i = 0; i < num_employee; i++){
        if(!(entry = get_entry_ptr(log, employee_list[i]))) return 0;
        records = realloc(records, (record_count + entry -> num_records) * sizeof(Record));
        for(j = 0; j < entry -> num_records; j++)
          records[record_count + j] = log -> record_list[entry -> indices[j]];
        record_count += entry -> num_records;
      }
      for(i = 0; i < num_guest; i++){
        if(!(entry = get_entry_ptr(log, guest_list[i]))) return 0;
        records = realloc(records, (record_count + entry -> num_records) * sizeof(Record));
        for(j = 0; j < entry -> num_records; j++)
          records[record_count + j] = log -> record_list[entry -> indices[j]];
        record_count += entry -> num_records;
      }
      qsort(records, record_count, sizeof(Record), record_cmp);

      free_log(log);
      log = new_log();
      unsigned int num_temp_rooms = 0;
      Room* temp_rooms = NULL;
      unsigned int num_result_rooms = 0;
      rooms = malloc(num_result_rooms * sizeof(Room));
      for(i = 0; i < record_count; i++){
        append_record(log, records[i]);
        num_temp_rooms = get_all_room_status(log, &temp_rooms);
        if(num_temp_rooms == 1 && temp_rooms[0].room_id != GALLERYID && temp_rooms[0].num_person == num_guest + num_employee){
          rooms = realloc(rooms, ++num_result_rooms * sizeof(Room));
          rooms[num_result_rooms - 1] = temp_rooms[0];
        }else
          free_rooms(temp_rooms, num_temp_rooms);
      }
      qsort(rooms, num_result_rooms, sizeof(Room), room_cmp);
      is_first = 1;
      for(i = 0; i < num_result_rooms; i++)
        if(i == 0 || rooms[i].room_id != rooms[i - 1].room_id){
          if(!is_first) printf(",");
          printf("%d", rooms[i].room_id);
        }
      if(num_result_rooms) printf("\n");
      free_rooms(rooms, num_result_rooms);
      break;
    default:
      invalid();
  }
  free(employee_list);
  free(guest_list);
  free_log(log);
  return 0;
}

int person_void_cmp(const void* person1, const void* person2){
  Person* p1 = (Person*) person1;
  Person* p2 = (Person*) person2;
  return strcmp(p1 -> name, p2 -> name);
}

int room_cmp(const void* room1, const void* room2){
  Room* r1 = (Room*) room1;
  Room* r2 = (Room*) room2;
  return r1 -> room_id - r2 -> room_id;
}

int record_cmp(const void* record1, const void* record2){
  Record* r1 = (Record*) record1;
  Record* r2 = (Record*) record2;
  if(r1 -> time == r2 -> time)
    return r1 -> action == r2 -> action &&
           r1 -> room == r2 -> room &&
           person_cmp(r1 -> person, r2 -> person);
  return r1 -> time - r2 -> time;
}
